#include <stdio.h>

int i = 102;
int j = -56;
long ix = -158693157400;
unsigned u = 35460;
float x = 12.687;
double dx = 0.000000025;
char c = 'C';


int main()
{
    printf("punto A:\n");
    printf("%4i \n",i);
    printf("%4i \n",j);
    printf("%14.8e \n",x);
    printf("%14.8e \n",dx);
    printf("punto B:\n%4i,%4i,%14.8e,%14.8e\n",i,j,x,dx);
    printf("punto C:\n%5i\n%12li\n%5i\n%10.5f\n%10.5d\n",i,ix,j,x,u);// este en realidad va separado como el punto A pero bue
    printf("punto D:\n%5i,%12li,%5i \n%10.5f,%10.5d\n",i,ix,j,x,u);
    printf("punto E:\n%6i   %6i   %c\n" ,i,u,c);
    printf("punto F:\n%5i  %5u  %11.4f\n" ,j,u,x);
    printf("punto G:\n%-5i  %-5u  %-11.4f\n" ,j,u,x);
    printf("punto H:\n%+5i  %5u  %+11.4f\n" ,j,u,x);
    printf("punto I:\n%05i  %05u  %011.4f\n" ,j,u,x);
    printf("punto J:\n%5i  %5u  %11.1f\n" ,j,u,x);
    return 0;
}
