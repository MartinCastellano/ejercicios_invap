#include <stdio.h>
#include <stdlib.h> 

void bbsort1(int array[], int n ) {
  
  for (int i = 0; i < n - 1; ++i) {
    
    for (int j = 0; j < n - i - 1; ++j) {
      
      if (abs(array[j]) >abs( array[j + 1])) {
        
        int temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
}
void bbsort2(int array[], int n ) {
  
  for (int i = 0; i < n - 1; ++i) {
    
    for (int j = 0; j < n - i - 1; ++j) {
      
      if (array[j] > array[j + 1]) {
        
        int temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
}

void bbsort3(int array[], int n ) {
  
  for (int i = 0; i < n - 1; ++i) {
    
    for (int j = 0; j < n - i - 1; ++j) {
      
      if (array[j] < array[j + 1]) {
        
        int temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
}

void bbsort4(int array[], int n ) {
  
  for (int i = 0; i < n - 1; ++i) {
    
    for (int j = 0; j < n - i - 1; ++j) {
      
      if (abs(array[j]) < abs( array[j + 1])) {
        
        int temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
}

void print_array(int array[], int size) {
  for (int i = 0; i < size; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

int main() {
  int x[] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};  
  int n = sizeof(x) / sizeof(x[0]);
  bbsort1(x, n);
  printf("'1' orden con valor abs:\n") ;   
  print_array(x, n);
  bbsort2(x, n);
  printf("'2' orden algebraico con signo:\n") ;
  print_array(x, n);
  bbsort3(x, n);
  printf("'3' orden con valor abs:\n") ;   
  print_array(x, n);
  bbsort4(x, n);
  printf("'4' orden algebraico con signo:\n") ;
  print_array(x, n);


}